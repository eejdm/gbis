function plotGPSDataModel(gps, geo, invpar, invResults, modelinput, saveName, saveflag, vis)

% Function to generate plot with comparison between GPS data and model
%
% Usage: plotGPSDataModel(gps, geo, invpar, invResults, modelinput, saveName, saveflag)
% =========================================================================
% This function is part of the:
% Geodetic Bayesian Inversion Software (GBIS)
% Software for the Bayesian inversion of geodetic data.
% Markov chain Monte Carlo algorithm incorporating the Metropolis alghoritm
% (e.g., Mosegaard & Tarantola, JGR,(1995).
%
% by Marco Bagnardi and Andrew Hooper (COMET, University of Leeds)
% Email: M.Bagnardi@leeds.ac.uk
% Reference: TBA (Bagnardi and Hooper, in prep.)
%
% The function uses third party software.
% =========================================================================
% Last update: 09/05/2017
%%

if nargin < 8
    vis='on';
end

global outputDir  % Set global variables

obsGPS = llh2local([gps.ll';zeros(1,length(gps.ll(:,1)))], geo.referencePoint)*1000; % Convert geographic coordinates to local cooridinates
obsGPS = [obsGPS; zeros(1,size(obsGPS,2))]; % Add zeros to third column of observation matrix
nObsGPS = size(obsGPS,2); % Determine number of entries in GPS observation matrix

% Display GPS vectors
obsGPS(:,end+1) = [max(obsGPS(1,:))+5000; min(obsGPS(2,:))-5000; 0]; % add coordinates of legend
scalebar = abs(round(max(gps.displacements(:))/3,3)); % Determine length of scalebar
gps.displacements(:,end+1) = [-scalebar 0 0]; % add "displacements" of legend

%% Generate plot

if isfield(geo,'faulttracefile')
    fault=dlmread(geo.faulttracefile);
    fault=llh2local([fault, zeros(size(fault(:,1)))]', geo.referencePoint)*1000;
else
    fault=[nan;nan];
end

figure('Position', [1, 1, 1200, 1000],'Visible',vis);
quiver(obsGPS(1,:), obsGPS(2,:), gps.displacements(1,:), gps.displacements(2,:), 1, ...
    'Color','k','LineWidth',1,'MaxHeadSize',0.03);%,'Marker','s')
axis equal;
ax = gca;
grid on
ax.Layer = 'top';
ax.Box = 'on';
ax.LineWidth = 1.5;
ax.GridLineStyle = '--';
xlabel('X distance from local origin (m)')
ylabel('Y distance from local origin (m)')
title('GPS horizontal displacements (data:black - model:red)')
xlim([min(obsGPS(1,:))-10000 max(obsGPS(1,:))+10000]);
ylim([min(obsGPS(2,:))-10000 max(obsGPS(2,:))+10000]);
text(obsGPS(1,end), obsGPS(2,end)-2000,[num2str(scalebar*1000),' mm']) % Add scalebar
drawnow
hold on
plot(fault(1,:),fault(2,:),'r','LineWidth',2)

obsGPS(:,end) = []; % remove coordinates of legend
gps.displacements(:,end) = []; % remove displacements for legend
gps.ve=gps.displacements(:,1);
gps.vn=gps.displacements(:,2);

%% Calculate forward model using optimal source parameters
faults= [find(strcmp(invpar.model,'FAUL')) find(strcmp(invpar.model,'DIKE')) find(strcmp(invpar.model,'SILL')) find(strcmp(invpar.model,'FHIN')) find(strcmp(invpar.model,'SPLT'))];
points= [find(strcmp(invpar.model,'MOGI')) find(strcmp(invpar.model,'MCTG')) find(strcmp(invpar.model,'YANG')) find(strcmp(invpar.model,'PENN'))];

modGPS = forwardGPSModel(obsGPS',invpar,invResults,modelinput); % Calculate modelled displacements
modGPSss.ve=modGPS(1,:);
modGPSss.vn=modGPS(2,:);


% Plot modelled displacements (Horizontal)
quiver(obsGPS(1,:),obsGPS(2,:),modGPS(1,:),modGPS(2,:),1,'Color','r','LineWidth',1,'MaxHeadSize',0.03)%,'Marker','s')

if  size(faults,2)>0
    for ii=1:size(faults,2)
        if strcmpi(invpar.model{ii},'FAUL') || strcmpi(invpar.model{ii},'DIKE') || strcmpi(invpar.model{ii},'SILL') || strcmpi(invpar.model{ii},'FHIN')
        center=invResults.optimalmodel{faults(ii)}(6:7);
        fault_top=[sind(invResults.optimalmodel{faults(ii)}(5))*0.5*invResults.optimalmodel{faults(ii)}(1),cosd(invResults.optimalmodel{faults(ii)}(5))*0.5*invResults.optimalmodel{faults(ii)}(1)];
        fault_base=[sind(invResults.optimalmodel{faults(ii)}(5)+90)*(cosd(invResults.optimalmodel{faults(ii)}(4))*invResults.optimalmodel{faults(ii)}(2)),cosd(invResults.optimalmodel{faults(ii)}(5)+90)*(cosd(invResults.optimalmodel{faults(ii)}(4))*invResults.optimalmodel{faults(ii)}(2))];
        plot([-fault_top(1), fault_top(1), fault_top(1)-fault_base(1),-fault_top(1)-fault_base(1),-fault_top(1)]+center(1),[-fault_top(2), fault_top(2),fault_top(2)-fault_base(2),-fault_top(2)-fault_base(2),-fault_top(2)]+center(2),'g','LineWidth',2)
        plot(invResults.optimalmodel{faults(ii)}(6),invResults.optimalmodel{faults(ii)}(7),'go','MarkerSize',10,'MarkerFaceColor','g')
        if strcmpi(invpar.model{ii},'FHIN')
            center2=mean([[fault_top(1)-fault_base(1),-fault_top(1)-fault_base(1)]+center(1);[fault_top(2)-fault_base(2),-fault_top(2)-fault_base(2)]+center(2)]');
            fault_base=[sind(invResults.optimalmodel{faults(ii)}(5)+90)*(cosd(invResults.optimalmodel{faults(ii)}(11))*invResults.optimalmodel{faults(ii)}(10)),cosd(invResults.optimalmodel{faults(ii)}(5)+90)*(cosd(invResults.optimalmodel{faults(ii)}(11))*invResults.optimalmodel{faults(ii)}(10))];
            plot([-fault_top(1), fault_top(1), fault_top(1)-fault_base(1),-fault_top(1)-fault_base(1),-fault_top(1)]+center2(1),[-fault_top(2), fault_top(2),fault_top(2)-fault_base(2),-fault_top(2)-fault_base(2),-fault_top(2)]+center2(2),'b--','LineWidth',2)
            plot(invResults.optimalmodel{faults(ii)}(6),invResults.optimalmodel{faults(ii)}(7),'g*','MarkerSize',10,'MarkerFaceColor','g')
        end
        end
        if strcmpi(invpar.model{ii},'SPLT') % using plotting from splitFault.m
            m=invResults.optimalmodel{1};
            % Coordinates of the split at the surface
            X_split = m(6); Y_split = m(7);
            
            % Global fault parameters
            L = m(1)/2; % Split assumed to be in center of the fault, so each fault has a length of L/2
            Strike = m(5); % Strike of the fault (will be same for both of the sections)
            Z = m(3); % Depth of fault tip (ie. locking depth)
            Y=0;
            
            % Parameters of one fault
            W1 = (m(2)/sind(-m(4)))-(m(3)/sind(-m(4))); % Width of slipping fault, given by distance surface to base of fault, minus distance surface to locking depth
            dip1 = m(4); % Dip of this portion of the fault
            X1= -(m(3)/tand(m(4)));% Midpoint of fault, before rotated off 090 (taking into account surface trace is not the midpoint
            
            % Parameters of other fault
            W2 = (m(2)/sind(-m(10)))-(m(3)/sind(-m(10))); % Width of slipping fault, given by distance surface to base of fault, minus distance surface to locking depth
            dip2 = m(10); % Dip of this portion of the fault
            X2= -(m(3)/tand(m(10)));% Midpoint of fault, before rotated off 090 (taking into account surface trace is not the midpoint
            
            % Create vector of Xs and Ys
            xs = [X1 X2]; ys = [Y Y];
            
            % Rotate coordinates
            xRot = xs*cosd(Strike) - ys*sind(Strike);
            yRot = xs*sind(Strike) + ys*cosd(Strike);
            
            % Combine parameters of new rotated Faults
            Fault3 = [L W1 Z dip1 Strike xRot(1)+X_split+sind(Strike-180)*L/2 yRot(1)+Y_split+cosd(Strike-180)*L/2 0 0 0];
            Fault4 = [L W2 Z dip2 Strike xRot(2)+X_split-sind(Strike-180)*L/2 yRot(2)+Y_split-cosd(Strike-180)*L/2 0 0 0];
            drawmodel(Fault3', 'color','r','projection','3D');
            drawmodel(Fault4', 'color','g','projection','3D');
        end
    end
end

if  size(points,2)>0
    for ii=1:size(points,2)
        plot(invResults.optimalmodel{points(ii)}(1),invResults.optimalmodel{points(ii)}(2),'b*','MarkerSize',10)
    end
end

if saveflag=='y'
    print(gcf,[outputDir,'/Figures/GPS_Data_Model_horizontal'],'-dpng')
end
drawnow

% Plot modelled displacements (Vertical)
figure('Position', [1, 1, 1200, 1000],'Visible',vis);
quiver(obsGPS(1,:), obsGPS(2,:), zeros(size(gps.displacements(1,:))), gps.displacements(3,:), 1, ...
    'Color','k','LineWidth',1,'MaxHeadSize',0.03);%,'Marker','s')
axis equal;
ax = gca;
grid on
ax.Layer = 'top';
ax.Box = 'on';
ax.LineWidth = 1.5;
ax.GridLineStyle = '--';
xlabel('X distance from local origin (m)')
ylabel('Y distance from local origin (m)')
title('GPS vertical displacements (data:black - model:red)')
xlim([min(obsGPS(1,:))-10000 max(obsGPS(1,:))+10000]);
ylim([min(obsGPS(2,:))-10000 max(obsGPS(2,:))+10000]);
text(obsGPS(1,end), obsGPS(2,end)-2000,[num2str(scalebar*1000),' mm']) % Add scalebar
drawnow
hold on
plot(fault(1,:),fault(2,:),'r','LineWidth',2)

quiver(obsGPS(1,:),obsGPS(2,:),zeros(size(modGPS(1,:))),modGPS(3,:),1,'Color','r','LineWidth',1,'MaxHeadSize',0.03)%,'Marker','s')
if  size(faults,2)>0
    for ii=1:size(faults,2)
        if strcmpi(invpar.model{ii},'FAUL') || strcmpi(invpar.model{ii},'DIKE') || strcmpi(invpar.model{ii},'SILL') || strcmpi(invpar.model{ii},'FHIN')
        center=invResults.optimalmodel{faults(ii)}(6:7);
        fault_top=[sind(invResults.optimalmodel{faults(ii)}(5))*0.5*invResults.optimalmodel{faults(ii)}(1),cosd(invResults.optimalmodel{faults(ii)}(5))*0.5*invResults.optimalmodel{faults(ii)}(1)];
        fault_base=[sind(invResults.optimalmodel{faults(ii)}(5)+90)*(cosd(invResults.optimalmodel{faults(ii)}(4))*invResults.optimalmodel{faults(ii)}(2)),cosd(invResults.optimalmodel{faults(ii)}(5)+90)*(cosd(invResults.optimalmodel{faults(ii)}(4))*invResults.optimalmodel{faults(ii)}(2))];
        plot([-fault_top(1), fault_top(1), fault_top(1)-fault_base(1),-fault_top(1)-fault_base(1),-fault_top(1)]+center(1),[-fault_top(2), fault_top(2),fault_top(2)-fault_base(2),-fault_top(2)-fault_base(2),-fault_top(2)]+center(2),'g','LineWidth',2)
        plot(invResults.optimalmodel{faults(ii)}(6),invResults.optimalmodel{faults(ii)}(7),'go','MarkerSize',10,'MarkerFaceColor','g')
        if strcmpi(invpar.model{ii},'FHIN')
            center2=mean([[fault_top(1)-fault_base(1),-fault_top(1)-fault_base(1)]+center(1);[fault_top(2)-fault_base(2),-fault_top(2)-fault_base(2)]+center(2)]');
            fault_base=[sind(invResults.optimalmodel{faults(ii)}(5)+90)*(cosd(invResults.optimalmodel{faults(ii)}(11))*invResults.optimalmodel{faults(ii)}(10)),cosd(invResults.optimalmodel{faults(ii)}(5)+90)*(cosd(invResults.optimalmodel{faults(ii)}(11))*invResults.optimalmodel{faults(ii)}(10))];
            plot([-fault_top(1), fault_top(1), fault_top(1)-fault_base(1),-fault_top(1)-fault_base(1),-fault_top(1)]+center2(1),[-fault_top(2), fault_top(2),fault_top(2)-fault_base(2),-fault_top(2)-fault_base(2),-fault_top(2)]+center2(2),'b--','LineWidth',2)
            plot(invResults.optimalmodel{faults(ii)}(6),invResults.optimalmodel{faults(ii)}(7),'g*','MarkerSize',10,'MarkerFaceColor','g')
        end
        end
        if strcmpi(invpar.model{ii},'SPLT') % using plotting from splitFault.m
            m=invResults.optimalmodel{1};
            % Coordinates of the split at the surface
            X_split = m(6); Y_split = m(7);
            
            % Global fault parameters
            L = m(1)/2; % Split assumed to be in center of the fault, so each fault has a length of L/2
            Strike = m(5); % Strike of the fault (will be same for both of the sections)
            Z = m(3); % Depth of fault tip (ie. locking depth)
            Y=0;
            
            % Parameters of one fault
            W1 = (m(2)/sind(-m(4)))-(m(3)/sind(-m(4))); % Width of slipping fault, given by distance surface to base of fault, minus distance surface to locking depth
            dip1 = m(4); % Dip of this portion of the fault
            X1= -(m(3)/tand(m(4)));% Midpoint of fault, before rotated off 090 (taking into account surface trace is not the midpoint
            
            % Parameters of other fault
            W2 = (m(2)/sind(-m(10)))-(m(3)/sind(-m(10))); % Width of slipping fault, given by distance surface to base of fault, minus distance surface to locking depth
            dip2 = m(10); % Dip of this portion of the fault
            X2= -(m(3)/tand(m(10)));% Midpoint of fault, before rotated off 090 (taking into account surface trace is not the midpoint
            
            % Create vector of Xs and Ys
            xs = [X1 X2]; ys = [Y Y];
            
            % Rotate coordinates
            xRot = xs*cosd(Strike) - ys*sind(Strike);
            yRot = xs*sind(Strike) + ys*cosd(Strike);
            
            % Combine parameters of new rotated Faults
            Fault3 = [L W1 Z dip1 Strike xRot(1)+X_split+sind(Strike-180)*L/2 yRot(1)+Y_split+cosd(Strike-180)*L/2 0 0 0];
            Fault4 = [L W2 Z dip2 Strike xRot(2)+X_split-sind(Strike-180)*L/2 yRot(2)+Y_split-cosd(Strike-180)*L/2 0 0 0];
            drawmodel(Fault3', 'color','r','projection','3D');
            drawmodel(Fault4', 'color','g','projection','3D');
        end
    end
end

if  size(points,2)>0
    for ii=1:size(points,2)
        plot(invResults.optimalmodel{points(ii)}(1),invResults.optimalmodel{points(ii)}(2),'b*','MarkerSize',10)
    end
end

if saveflag=='y'
    print(gcf,[outputDir,'/Figures/GPS_Data_Model_vertical'],'-dpng')
end
drawnow


%% Plot contour

figure('Position', [1, 1, 1200, 1000],'Visible',vis);
plot(obsGPS(1,:), obsGPS(2,:),'k.');
axis equal;
ax = gca;
grid on
ax.Layer = 'top';
ax.Box = 'on';
ax.LineWidth = 1.5;
ax.GridLineStyle = '--';
xlabel('X distance from local origin (m)')
ylabel('Y distance from local origin (m)')
title('Modelled GPS vertical displacements (mm/yr)')
xlim([min(obsGPS(1,:))-10000 max(obsGPS(1,:))+10000]);
ylim([min(obsGPS(2,:))-10000 max(obsGPS(2,:))+10000]);
hold on
plot(fault(1,:),fault(2,:),'r','LineWidth',2)

%%
rh=max(modGPS(3,:)*1000);

if rh>1000
    b=100;
elseif rh>100
    b=10;
elseif rh>50
    b=5;
elseif rh>10
    b=1;
elseif rh>5;
    b=0.5;
elseif rh>1;
    b=0.25;
else
    b=0.1;
end

lines=floor(min(modGPS(3,:))*1000):b:ceil(max(modGPS(3,:)*1000));


Xmat=[min(obsGPS(1,:)):100:max(obsGPS(1,:))];
Ymat=[min(obsGPS(2,:)):100:max(obsGPS(2,:))];
[Xmat,Ymat]=meshgrid(Xmat,Ymat);
Z=scatteredInterpolant(obsGPS(1,:)',obsGPS(2,:)',(modGPS(3,:)*1000)');
Zmat=Z(Xmat,Ymat);
c=convhull(obsGPS(1,:),obsGPS(2,:));
[in,on]=inpolygon(Xmat,Ymat,obsGPS(1,c),obsGPS(2,c));
out=((in+on)==0);
Zmat(out)=NaN;
[m,C]=contour(Xmat,Ymat,Zmat,lines);
clabel(m,C,lines(1:3:end))
colorbar
drawnow
%%
if  size(faults,2)>0
    for ii=1:size(faults,2)
        if strcmpi(invpar.model{ii},'FAUL') || strcmpi(invpar.model{ii},'DIKE') || strcmpi(invpar.model{ii},'SILL') || strcmpi(invpar.model{ii},'FHIN')
        center=invResults.optimalmodel{faults(ii)}(6:7);
        fault_top=[sind(invResults.optimalmodel{faults(ii)}(5))*0.5*invResults.optimalmodel{faults(ii)}(1),cosd(invResults.optimalmodel{faults(ii)}(5))*0.5*invResults.optimalmodel{faults(ii)}(1)];
        fault_base=[sind(invResults.optimalmodel{faults(ii)}(5)+90)*(cosd(invResults.optimalmodel{faults(ii)}(4))*invResults.optimalmodel{faults(ii)}(2)),cosd(invResults.optimalmodel{faults(ii)}(5)+90)*(cosd(invResults.optimalmodel{faults(ii)}(4))*invResults.optimalmodel{faults(ii)}(2))];
        plot([-fault_top(1), fault_top(1), fault_top(1)-fault_base(1),-fault_top(1)-fault_base(1),-fault_top(1)]+center(1),[-fault_top(2), fault_top(2),fault_top(2)-fault_base(2),-fault_top(2)-fault_base(2),-fault_top(2)]+center(2),'g','LineWidth',2)
        plot(invResults.optimalmodel{faults(ii)}(6),invResults.optimalmodel{faults(ii)}(7),'go','MarkerSize',10,'MarkerFaceColor','g')
        if strcmpi(invpar.model{ii},'FHIN')
            center2=mean([[fault_top(1)-fault_base(1),-fault_top(1)-fault_base(1)]+center(1);[fault_top(2)-fault_base(2),-fault_top(2)-fault_base(2)]+center(2)]');
            fault_base=[sind(invResults.optimalmodel{faults(ii)}(5)+90)*(cosd(invResults.optimalmodel{faults(ii)}(11))*invResults.optimalmodel{faults(ii)}(10)),cosd(invResults.optimalmodel{faults(ii)}(5)+90)*(cosd(invResults.optimalmodel{faults(ii)}(11))*invResults.optimalmodel{faults(ii)}(10))];
            plot([-fault_top(1), fault_top(1), fault_top(1)-fault_base(1),-fault_top(1)-fault_base(1),-fault_top(1)]+center2(1),[-fault_top(2), fault_top(2),fault_top(2)-fault_base(2),-fault_top(2)-fault_base(2),-fault_top(2)]+center2(2),'b--','LineWidth',2)
            plot(invResults.optimalmodel{faults(ii)}(6),invResults.optimalmodel{faults(ii)}(7),'g*','MarkerSize',10,'MarkerFaceColor','g')
        end
        end
        if strcmpi(invpar.model{ii},'SPLT') % using plotting from splitFault.m
            m=invResults.optimalmodel{faults(ii)};
            % Coordinates of the split at the surface
            X_split = m(6); Y_split = m(7);
            
            % Global fault parameters
            L = m(1)/2; % Split assumed to be in center of the fault, so each fault has a length of L/2
            Strike = m(5); % Strike of the fault (will be same for both of the sections)
            Z = m(3); % Depth of fault tip (ie. locking depth)
            Y=0;
            
            % Parameters of one fault
            W1 = (m(2)/sind(-m(4)))-(m(3)/sind(-m(4))); % Width of slipping fault, given by distance surface to base of fault, minus distance surface to locking depth
            dip1 = m(4); % Dip of this portion of the fault
            X1= -(m(3)/tand(m(4)));% Midpoint of fault, before rotated off 090 (taking into account surface trace is not the midpoint
            
            % Parameters of other fault
            W2 = (m(2)/sind(-m(10)))-(m(3)/sind(-m(10))); % Width of slipping fault, given by distance surface to base of fault, minus distance surface to locking depth
            dip2 = m(10); % Dip of this portion of the fault
            X2= -(m(3)/tand(m(10)));% Midpoint of fault, before rotated off 090 (taking into account surface trace is not the midpoint
            
            % Create vector of Xs and Ys
            xs = [X1 X2]; ys = [Y Y];
            
            % Rotate coordinates
            xRot = xs*cosd(Strike) - ys*sind(Strike);
            yRot = xs*sind(Strike) + ys*cosd(Strike);
            
            % Combine parameters of new rotated Faults
            Fault3 = [L W1 Z dip1 Strike xRot(1)+X_split+sind(Strike-180)*L/2 yRot(1)+Y_split+cosd(Strike-180)*L/2 0 0 0];
            Fault4 = [L W2 Z dip2 Strike xRot(2)+X_split-sind(Strike-180)*L/2 yRot(2)+Y_split-cosd(Strike-180)*L/2 0 0 0];
            drawmodel(Fault3', 'color','g');
            drawmodel(Fault4', 'color','g');
            plot(m(6),m(7),'g*','MarkerSize',10,'MarkerFaceColor','g')
        end
    end
end

if  size(points,2)>0
    for ii=1:size(points,2)
        plot(invResults.optimalmodel{points(ii)}(1),invResults.optimalmodel{points(ii)}(2),'b*','MarkerSize',10)
    end
end

if saveflag=='y'
    print(gcf,[outputDir,'/Figures/GPS_Contour_Model_vertical'],'-dpng')
end
drawnow

%% Plot residual contour

figure('Position', [1, 1, 1200, 1000],'Visible',vis);

axis equal;
ax = gca;
grid on
ax.Layer = 'top';
ax.Box = 'on';
ax.LineWidth = 1.5;
ax.GridLineStyle = '--';
xlabel('X distance from local origin (m)')
ylabel('Y distance from local origin (m)')
title('Residual Displacements (Obs-Mod) (mm/yr)')
xlim([min(obsGPS(1,:))-10000 max(obsGPS(1,:))+10000]);
ylim([min(obsGPS(2,:))-10000 max(obsGPS(2,:))+10000]);
hold on


%%
vresid=(gps.displacements(3,:)-modGPS(3,:))*1000;
rh=range(vresid);

if rh>1000
    b=100;
elseif rh>100
    b=10;
elseif rh>50
    b=5;
elseif rh>10
    b=2;
elseif rh>5;
    b=1;
elseif rh>1;
    b=0.5;
else
    b=0.1;
end

lines=round(min(vresid)):b:round(max(vresid));

Xrange=[min(obsGPS(1,:)):100:max(obsGPS(1,:))];
Yrange=[min(obsGPS(2,:)):100:max(obsGPS(2,:))];
[Xmat,Ymat]=meshgrid(Xrange,Yrange);
Z=scatteredInterpolant(obsGPS(1,:)',obsGPS(2,:)',vresid');
Zmat=Z(Xmat,Ymat);
c=convhull(obsGPS(1,:),obsGPS(2,:));
[in,on]=inpolygon(Xmat,Ymat,obsGPS(1,c),obsGPS(2,c));
out=((in+on)==0);
Zmat(out)=NaN;
A=imagesc(Xrange,Yrange,Zmat);
set(A,'AlphaData',~isnan(Zmat));


if  size(faults,2)>0
    for ii=1:size(faults,2)
        if strcmpi(invpar.model{ii},'FAUL') || strcmpi(invpar.model{ii},'DIKE') || strcmpi(invpar.model{ii},'SILL') || strcmpi(invpar.model{ii},'FHIN')
            center=invResults.optimalmodel{faults(ii)}(6:7);
            fault_top=[sind(invResults.optimalmodel{faults(ii)}(5))*0.5*invResults.optimalmodel{faults(ii)}(1),cosd(invResults.optimalmodel{faults(ii)}(5))*0.5*invResults.optimalmodel{faults(ii)}(1)];
            fault_base=[sind(invResults.optimalmodel{faults(ii)}(5)+90)*(cosd(invResults.optimalmodel{faults(ii)}(4))*invResults.optimalmodel{faults(ii)}(2)),cosd(invResults.optimalmodel{faults(ii)}(5)+90)*(cosd(invResults.optimalmodel{faults(ii)}(4))*invResults.optimalmodel{faults(ii)}(2))];
            plot([-fault_top(1), fault_top(1), fault_top(1)-fault_base(1),-fault_top(1)-fault_base(1),-fault_top(1)]+center(1),[-fault_top(2), fault_top(2),fault_top(2)-fault_base(2),-fault_top(2)-fault_base(2),-fault_top(2)]+center(2),'g','LineWidth',2)
            plot(invResults.optimalmodel{faults(ii)}(6),invResults.optimalmodel{faults(ii)}(7),'go','MarkerSize',10,'MarkerFaceColor','g')
            if strcmpi(invpar.model{ii},'FHIN')
                center2=mean([[fault_top(1)-fault_base(1),-fault_top(1)-fault_base(1)]+center(1);[fault_top(2)-fault_base(2),-fault_top(2)-fault_base(2)]+center(2)]');
                fault_base=[sind(invResults.optimalmodel{faults(ii)}(5)+90)*(cosd(invResults.optimalmodel{faults(ii)}(11))*invResults.optimalmodel{faults(ii)}(10)),cosd(invResults.optimalmodel{faults(ii)}(5)+90)*(cosd(invResults.optimalmodel{faults(ii)}(11))*invResults.optimalmodel{faults(ii)}(10))];
                plot([-fault_top(1), fault_top(1), fault_top(1)-fault_base(1),-fault_top(1)-fault_base(1),-fault_top(1)]+center2(1),[-fault_top(2), fault_top(2),fault_top(2)-fault_base(2),-fault_top(2)-fault_base(2),-fault_top(2)]+center2(2),'b--','LineWidth',2)
                plot(invResults.optimalmodel{faults(ii)}(6),invResults.optimalmodel{faults(ii)}(7),'g*','MarkerSize',10,'MarkerFaceColor','g')
            end
        end
        if strcmpi(invpar.model{ii},'SPLT') % using plotting from splitFault.m
            m=invResults.optimalmodel{faults(ii)};
            % Coordinates of the split at the surface
            X_split = m(6); Y_split = m(7);
            
            % Global fault parameters
            L = m(1)/2; % Split assumed to be in center of the fault, so each fault has a length of L/2
            Strike = m(5); % Strike of the fault (will be same for both of the sections)
            Z = m(3); % Depth of fault tip (ie. locking depth)
            Y=0;
            
            % Parameters of one fault
            W1 = (m(2)/sind(-m(4)))-(m(3)/sind(-m(4))); % Width of slipping fault, given by distance surface to base of fault, minus distance surface to locking depth
            dip1 = m(4); % Dip of this portion of the fault
            X1= -(m(3)/tand(m(4)));% Midpoint of fault, before rotated off 090 (taking into account surface trace is not the midpoint
            
            % Parameters of other fault
            W2 = (m(2)/sind(-m(10)))-(m(3)/sind(-m(10))); % Width of slipping fault, given by distance surface to base of fault, minus distance surface to locking depth
            dip2 = m(10); % Dip of this portion of the fault
            X2= -(m(3)/tand(m(10)));% Midpoint of fault, before rotated off 090 (taking into account surface trace is not the midpoint
            
            % Create vector of Xs and Ys
            xs = [X1 X2]; ys = [Y Y];
            
            % Rotate coordinates
            xRot = xs*cosd(Strike) - ys*sind(Strike);
            yRot = xs*sind(Strike) + ys*cosd(Strike);
            
            % Combine parameters of new rotated Faults
            Fault3 = [L W1 Z dip1 Strike xRot(1)+X_split+sind(Strike-180)*L/2 yRot(1)+Y_split+cosd(Strike-180)*L/2 0 0 0];
            Fault4 = [L W2 Z dip2 Strike xRot(2)+X_split-sind(Strike-180)*L/2 yRot(2)+Y_split-cosd(Strike-180)*L/2 0 0 0];
            drawmodel(Fault3', 'color','g','projection','no');
            drawmodel(Fault4', 'color','g','projection','no');
            plot(m(6),m(7),'g*','MarkerSize',10,'MarkerFaceColor','g')
        end
    end
end

plot(fault(1,:),fault(2,:),'r','LineWidth',2)
[m,C]=contour(Xmat,Ymat,Zmat,lines,'-black');
clabel(m,C,lines(1:2:end))
colorbar
plot(obsGPS(1,:), obsGPS(2,:),'k.');

load('~/scripts/cpts/vik/vik.mat')
[vik]=crop_cmap(vik,[min(Zmat(:)) max(Zmat(:))],0);
colormap(vik)

if  size(points,2)>0
    for ii=1:size(points,2)
        plot(invResults.optimalmodel{points(ii)}(1),invResults.optimalmodel{points(ii)}(2),'b*','MarkerSize',10)
    end
end

if saveflag=='y'
    print(gcf,[outputDir,'/Figures/GPS_Contour_Resid_vertical'],'-dpng')
end

drawnow

%% Plot Pointcloud

figure('Visible',vis);
hold on
title('Observed vs Modelled GPS')
xlabel('GPS Observation (mm)')
ylabel('Modelled GPS (mm)')
plot(gps.displacements(1,:)*1e3,modGPS(1,:)*1e3,'r.')
plot(gps.displacements(2,:)*1e3,modGPS(2,:)*1e3,'b.')
plot(gps.displacements(3,:)*1e3,modGPS(3,:)*1e3,'g.')
legend('East','North','Up','Location','NorthWest','Autoupdate','off')
modrange=[floor(min(modGPS(:)*1e3)) ceil(max(modGPS(:)*1e3))];
obsrange=[floor(min(gps.displacements(:)*1e3)) ceil(max(gps.displacements(:)*1e3))];
xlim(obsrange)
ylim(modrange)
plot([-1e3 1e3],[-1e3 1e3],'k--') 

if saveflag=='y'
    print(gcf,[outputDir,'/Figures/GPS_Data_Model_scatter'],'-dpng')
end
drawnow

%% Plot histogram

vresid=(gps.displacements-modGPS)*1000;
rh=range(vresid(:));

if rh>1000
    b=100;
elseif rh>100
    b=10;
elseif rh>50
    b=5;
elseif rh>10
    b=1;
elseif rh>5;
    b=0.5;
elseif rh>1;
    b=0.25;
else
    b=0.1;
end

figure('Visible',vis);
hold on

E_edges=round(min(vresid(1,:)):b:max(vresid(1,:)));
N_edges=round(min(vresid(2,:)):b:max(vresid(2,:)));
U_edges=round(min(vresid(3,:)):b:max(vresid(3,:)));

histogram(vresid(1,:),E_edges)
histogram(vresid(2,:),N_edges)
histogram(vresid(3,:),U_edges)

legend(sprintf('East- Mean: %.1f, Std: %.1f',mean(vresid(1,:)),std(vresid(1,:))),...
    sprintf('North- Mean: %.1f, Std: %.1f',mean(vresid(2,:)),std(vresid(2,:))),...
    sprintf('Up- Mean: %.1f, Std: %.1f',mean(vresid(3,:)),std(vresid(3,:))),'Location','NorthWest')

xlabel('Residual (mm)')
title('Residual: Observed GPS - Modelled GPS')

if saveflag=='y'
    print(gcf,[outputDir,'/Figures/GPS_Data_Model_hist'],'-dpng')
end

drawnow

