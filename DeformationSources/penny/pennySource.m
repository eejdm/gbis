function U = pennySource(m,obs,nu)

% Function that adapts input and output to use fialkodisp.m from dMODELS
% References ***************************************************************
% Fialko, Y, Khazan, Y and M. Simons (2001). Deformation due to a 
% pressurized horizontal circular crack in an elastic half-space, with 
% applications to volcano geodesy. Geophys. J. Int., 146, 181�190
% 
% Battaglia, M., Cervelli, P.F., Murray, J.R., 2013a.dMODELS: A MATLAB software 
% package for modeling crustal deformation near active faults and volcanic centers.
% Journal of Volcanology and Geothermal Research 254, 1?4.
% 
% Battaglia, M., Cervelli, P.F., Murray-Muraleda, J.R., 2013b. Modeling 
% crustal deformation?A catalog of deformation models and modeling approaches. 
% U.S. Geological Survey Techniques and Methods, book 13, chap. B1, 96 p.
% =========================================================================
% This function is part of the:
% Geodetic Bayesian Inversion Software (GBIS)
% Software for the Bayesian inversion of geodetic data.
% Markov chain Monte Carlo algorithm incorporating the Metropolis alghoritm
% (e.g., Mosegaard & Tarantola, JGR,(1995).
%
% by Marco Bagnardi and Andrew Hooper (COMET, University of Leeds)
% Email: M.Bagnardi@leeds.ac.uk
% Reference: TBA (Bagnardi and Hooper, in prep.)
%
% The function uses third party software.
% =========================================================================
% Last update: 10/05/2017
%%
% Assign values in m to model parameters
x0 = m(1);
y0 = m(2);
z0 = m(3);
P_G = m(5);
a = m(4);

% Assign obs to x and y
x = obs(1,:);
y = obs(2,:);
z = 0;

% Calculate displacements
[u v w dV] = fialkodisp(x0,y0,z0,P_G,a,nu,x,y,z);

% Combind 3D displacements in U matrix
U = [u;v;w;];